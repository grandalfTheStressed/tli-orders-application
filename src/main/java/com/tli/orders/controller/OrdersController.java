package com.tli.orders.controller;

import com.tli.orders.dto.OrderDto;
import com.tli.orders.dto.OrderLineItemsDto;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrdersController extends BaseController{

    @GetMapping("/{orderId}")
    public Object getOrders(@PathVariable("orderId") long id){

        return ordersService.getById(id);
    }

    @DeleteMapping("/{orderId}")
    public void deleteOrder(@PathVariable("orderId") Long orderId){

        ordersService.delete(orderId);
    }

    @DeleteMapping("/item/{orderId}/{number}")
    public void deleteOrderItem(@PathVariable("orderId") Long orderId, @PathVariable("number") Integer number){

        OrderLineItemsDto orderLineItemsDto = new OrderLineItemsDto();
        orderLineItemsDto.setOrderId(orderId);
        orderLineItemsDto.setNumber(number);
        orderLineItemsDto.setQuantity(0);

        orderLineItemsService.update(orderLineItemsDto);
    }

    /*ex.
    {
        "orderId": 1,
        "number": 1,
        "name": "Widget",
        "price": 20.0,
        "quantity": 3
     }
     */
    @PutMapping
    public void updateOrder(@RequestBody OrderLineItemsDto orderLineItemsDto){

        orderLineItemsService.update(orderLineItemsDto);
    }

    /*ex.
    {
        "customerId": 1,
        "editor": 1,
        "items": [
            {
                "name": "Widget",
                "price": 20.00,
                "quantity": 1
            },
            {
                "name": "Widget",
                "price": 35.00,
                "quantity": 2
            }
        ]
     }
     */
    @PostMapping
    public void createOrder(@RequestBody OrderDto orderDto){

        ordersService.save(orderDto);
    }
}
