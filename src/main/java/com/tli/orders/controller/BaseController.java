package com.tli.orders.controller;

import com.tli.orders.service.CustomersService;
import com.tli.orders.service.OrderLineItemsService;
import com.tli.orders.service.OrderStatusesService;
import com.tli.orders.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {

    @Autowired
    OrderStatusesService orderStatusesService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    OrderLineItemsService orderLineItemsService;

    @Autowired
    CustomersService customersService;
}
