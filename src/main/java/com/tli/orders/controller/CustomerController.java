package com.tli.orders.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tli.orders.dto.CustomersDto;
import com.tli.orders.dto.OrderLineItemsDto;
import com.tli.orders.dto.OrderSummaryDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController extends BaseController{

    /*ex.
    {
        "editor": 1,
        "name": "Grant Fields",
        "address": "Akin Ln"
     }
     */
    @PostMapping
    public void createCustomer(@RequestBody CustomersDto customersDto){
        customersService.save(customersDto);
    }

    @GetMapping
    public Object getCustomers(){

        return customersService.getAll();
    }

    @GetMapping("/{customerId}")
    public Object getCustomer(@PathVariable("customerId") long id){

        return customersService.getById(id);
    }

    @GetMapping("/{customerId}/orders")
    public List<OrderSummaryDto> getOrdersByCustomer(@PathVariable("customerId") long id){

        return ordersService.getAllByCustomerId(id);
    }

    /*ex.
    {
        "editor": 1,
        "customerId": 1,
        "name": "Grant Fields",
        "address": "Akin Ln"
     }
     */
    @PutMapping
    public void updateCustomer(@RequestBody CustomersDto customersDto){

        customersService.update(customersDto);
    }
}
