package com.tli.orders.service;

import com.tli.orders.dao.OrdersDao;
import com.tli.orders.dto.OrderDto;
import com.tli.orders.dto.OrderLineItemsDto;
import com.tli.orders.dto.OrderSummaryDto;
import com.tli.orders.model.Customers;
import com.tli.orders.model.OrderLineItems;
import com.tli.orders.model.OrderStatuses;
import com.tli.orders.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrdersService {

    @Autowired
    OrdersDao ordersDao;

    @Autowired
    CustomersService customersService;

    @Autowired
    OrderStatusesService orderStatusesService;

    @Transactional
    public void save(Orders orders){
        ordersDao.save(orders);
    }

    @Transactional
    public void save(OrderDto orderDto){

        Customers customer = customersService.getById(orderDto.getCustomersId());

        if(customer == null) throw new NullPointerException();

        Integer creator = orderDto.getEditor();

        List<OrderLineItemsDto> orderListDto = orderDto.getOrderList();
        List<OrderLineItems> orderList = new ArrayList<>();

        Orders orders = new Orders(creator);
        orders.setCustomer(customer);
        orders.setOrderStatus(orderStatusesService.getByName("New"));

        for(int i = 0; i < orderListDto.size(); i++){
            OrderLineItemsDto temp = orderListDto.get(i);

            OrderLineItems orderLineItems = new OrderLineItems(creator);
            orderLineItems.setNumber(i + 1);
            orderLineItems.setName(temp.getName());
            orderLineItems.setPrice(temp.getPrice());
            orderLineItems.setQuantity(temp.getQuantity());
            orderLineItems.setOrderId(orders);
            orderList.add(orderLineItems);
        }

        orders.setOrderLineItems(orderList);
        ordersDao.save(orders);
    }

    @Transactional
    public void delete(Long orderId){
        if(orderId == null) throw new NullPointerException();

        Orders order = ordersDao.getById(orderId);

        OrderStatuses orderStatuses = orderStatusesService.getByName("Canceled");
        order.setOrderStatus(orderStatuses);
        ordersDao.save(order);
    }

    public List<OrderSummaryDto> getAllByCustomerId(long id){

        List<OrderSummaryDto> orderSummary = new ArrayList<>();

        for(Orders orders: ordersDao.findAllByCustomerId(id)){
            orderSummary.add(new OrderSummaryDto(orders));
        }

        return orderSummary;
    }

    public Orders getById(long id){
        return ordersDao.getById(id);
    }
}
