package com.tli.orders.service;

import com.tli.orders.dao.CustomersDao;
import com.tli.orders.dto.CustomersDto;
import com.tli.orders.model.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CustomersService {

    @Autowired
    CustomersDao customersDao;

    public List<Customers> getAll(){

        return customersDao.findAll();
    }

    public Customers getById(long id){

        return customersDao.getById(id);
    }

    public void save(CustomersDto customersDto){

        Customers customer = new Customers(customersDto.getEditor());
        customer.setName(customersDto.getName());
        customer.setAddress(customersDto.getAddress());
        customersDao.save(customer);
    }

    public void update(CustomersDto customersDto){

        Customers customer = getById(customersDto.getId());

        if(customer != null){
            customer.setModifiedBy(customersDto.getEditor());
            customer.setModifiedDate(new Date(System.nanoTime()));
            customer.setName(customersDto.getName());
            customer.setAddress(customer.getAddress());
        }
        else throw new NullPointerException();
    }
}
