package com.tli.orders.service;

import com.tli.orders.dao.OrderStatusesDao;
import com.tli.orders.model.OrderStatuses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderStatusesService {

    @Autowired
    OrderStatusesDao orderStatusesDao;

    public OrderStatuses getByName(String name){
        return orderStatusesDao.getByName(name);
    }
}
