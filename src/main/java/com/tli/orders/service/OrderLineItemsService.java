package com.tli.orders.service;

import com.tli.orders.dao.OrderLineItemsDao;
import com.tli.orders.dto.OrderLineItemsDto;
import com.tli.orders.model.OrderLineItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderLineItemsService {

    @Autowired
    private OrderLineItemsDao orderLineItemsDao;

    @Autowired
    private OrdersService ordersService;

    @Transactional
    public void update(OrderLineItemsDto orderLineItemsDto){

        if(orderLineItemsDto.getNumber() == null || orderLineItemsDto.getOrderId() == null) throw new NullPointerException();

        OrderLineItems orderLineItems = orderLineItemsDao.findByOrderAndNumber(ordersService.getById(orderLineItemsDto.getOrderId()), orderLineItemsDto.getNumber());

        if(orderLineItems == null) throw new NullPointerException();

        if(orderLineItemsDto.getQuantity() > 0) {
            orderLineItems.setQuantity(orderLineItemsDto.getQuantity());
        }
        else{
            orderLineItems.getOrderId().getOrderLineItems().remove(orderLineItems.getNumber() - 1);
            ordersService.save(orderLineItems.getOrderId());
            orderLineItemsDao.deleteById(orderLineItems.getId());
            return;
        }

        if(orderLineItemsDto.getPrice() >= 0)
            orderLineItems.setPrice(orderLineItemsDto.getPrice());

        orderLineItems.getOrderId().getOrderLineItems().set(orderLineItems.getNumber() - 1, orderLineItems);
        ordersService.save(orderLineItems.getOrderId());
        orderLineItemsDao.save(orderLineItems);
    }
}
