package com.tli.orders.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class OrderDto {

    private Long customerId;
    private Integer editor;
    private List<OrderLineItemsDto> orderList;

    public OrderDto(
            @JsonProperty("customerId") Long customerId,
            @JsonProperty("editor") Integer editor,
            @JsonProperty("items") List<OrderLineItemsDto> orderList){
        this.customerId = customerId;
        this.editor = editor;
        this.orderList = orderList;
    }

    public Long getCustomersId() {
        return customerId;
    }

    public void setCustomersId(Long customersDto) {
        this.customerId = customersDto;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getEditor() {
        return editor;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public List<OrderLineItemsDto> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderLineItemsDto> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
