package com.tli.orders.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tli.orders.model.OrderLineItems;
import com.tli.orders.model.Orders;

import java.util.ArrayList;
import java.util.List;

public class OrderSummaryDto {

    private long orderId;
    private String status;
    private List<OrderLineItemsDto> orderList;

    public OrderSummaryDto(Orders order){

        orderId = order.getId();
        status = order.getOrderStatus().getName();
        orderList = new ArrayList<>();

        for(OrderLineItems orderLine: order.getOrderLineItems()){

            OrderLineItemsDto temp = new OrderLineItemsDto(orderLine);
            orderList.add(temp);
        }
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderLineItemsDto> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderLineItemsDto> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
