package com.tli.orders.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tli.orders.model.OrderLineItems;

public class OrderLineItemsDto {

    private Long orderId;
    private Integer number;
    private String name;
    private Double price;
    private Integer quantity;

    public OrderLineItemsDto(OrderLineItems orderLineItems){

        orderId = orderLineItems.getId();
        number = orderLineItems.getNumber();
        name = orderLineItems.getName();
        price = orderLineItems.getPrice();
        quantity = orderLineItems.getQuantity();
    }

    public OrderLineItemsDto(){}

    public OrderLineItemsDto(
            @JsonProperty("orderId") Long orderId,
            @JsonProperty("number") Integer number,
            @JsonProperty("name") String name,
            @JsonProperty("price") Double price,
            @JsonProperty("quantity") Integer quantity){
        this.orderId = orderId;
        this.number = number;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
