package com.tli.orders.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CustomersDto {

    private Long id;
    private String name;
    private String address;
    private Integer editor;

    public CustomersDto(){}

    public CustomersDto(
            @JsonProperty("id") Long id,
            @JsonProperty("name") String name,
            @JsonProperty("address") String address,
            @JsonProperty("editor") Integer editor){
        this.id = id;
        this.name = name;
        this.address = address;
        this.editor = editor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getEditor() {
        return editor;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
