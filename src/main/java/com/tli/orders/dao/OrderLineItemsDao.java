package com.tli.orders.dao;

import com.tli.orders.model.OrderLineItems;
import com.tli.orders.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderLineItemsDao extends JpaRepository<OrderLineItems, Long> {

    OrderLineItems findByOrderAndNumber(Orders order, Integer Number);
}
