package com.tli.orders.dao;

import com.tli.orders.model.Customers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomersDao extends JpaRepository<Customers, Long> {
}
