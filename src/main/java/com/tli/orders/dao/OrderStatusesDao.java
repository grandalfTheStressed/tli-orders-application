package com.tli.orders.dao;

import com.tli.orders.model.OrderStatuses;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderStatusesDao extends JpaRepository<OrderStatuses, Long> {

    OrderStatuses getByName(String name);
}
