package com.tli.orders.dao;

import com.tli.orders.model.OrderStatuses;
import com.tli.orders.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersDao extends JpaRepository<Orders, Long> {

    Iterable<Orders> findAllByCustomerId(Long id);
}
